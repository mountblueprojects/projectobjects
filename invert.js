function invert(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }

  const newObject = {};

  for (key in object) {
    newObject[object[key]] = key;
  }

  return newObject;
}

module.exports = invert;
