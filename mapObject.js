function mapObject(object, callback) {
  if (
    typeof object !== "object" ||
    Array.isArray(object) ||
    typeof callback !== "function"
  ) {
    return [];
  }

  for (key in object) {
    object[key] = callback(object[key]);
  }

  return object;
}

module.exports = mapObject;
