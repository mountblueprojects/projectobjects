const arr = ["a", "b", "c"];
const keys = require("../keys");
const result = keys(arr);

if (result.length === 0) {
  console.log("Please enter an object with properties");
} else {
  console.log(result);
}
