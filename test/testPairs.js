const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const pairs = require("../pairs");
const result = pairs(testObject);

if (result.length === 0) {
  console.log("Please enter an object with values");
} else {
  console.log(result);
}
