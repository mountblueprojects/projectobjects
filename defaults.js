function defaults(object, defaultPropertiesObject) {
  if (
    typeof object !== "object" ||
    typeof defaultPropertiesObject !== "object" ||
    Array.isArray(object) ||
    Array.isArray(defaultPropertiesObject)
  ) {
    return [];
  }

  for (key in defaultPropertiesObject) {
    if (object[key] === undefined) {
      object[key] = defaultPropertiesObject[key];
    }
  }

  return object;
}
module.exports = defaults;
