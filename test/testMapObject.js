testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const mapObject = require("../mapObject");

function add(value) {
  return value + 5;
}

const result = mapObject(testObject, add);

if (result.length === 0) {
  console.log(
    "Please enter an object with values and/or enter a proper callback function"
  );
} else {
  console.log(result);
}
