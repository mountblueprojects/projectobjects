function pairs(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }

  const pairs = [];

  for (key in object) {
    pairs.push([key, object[key]]);
  }

  return pairs;
}

module.exports = pairs;
