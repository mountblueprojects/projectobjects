function values(object) {
  if (typeof object !== "object" || Array.isArray(object)) {
    return [];
  }

  const valueArray = [];

  for (key in object) {
    if (typeof object[key] === "function") {
      continue;
    }
    valueArray.push(object[key]);
  }

  return valueArray;
}

module.exports = values;
