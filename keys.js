function keys(object) {
  if (typeof object !== "object") {
    return [];
  }

  const keyArray = [];

  for (key in object) {
    keyArray.push(key);
  }

  return keyArray;
}

module.exports = keys;
