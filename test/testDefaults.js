const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };
const newObject = { name: "Clark Kent", profession: "Reporter" };

const defaults = require("../defaults");
const result = defaults(testObject, newObject);
console.log(result);
